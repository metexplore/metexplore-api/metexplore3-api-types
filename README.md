# MetExplore 3 API Typescript types

- Build automatically typescript types from MetExplore 3 Api specifications.
- the creation of the npm package is done the CI when push to the master branch
- npm package is downloadable [here](https://forgemia.inra.fr/metexplore/metexplore-api/metexplore3-api-types/-/packages)

## How to use it?

Create a .npmrc file:

```.npmrc
@metexplore:registry=https://forgemia.inra.fr/api/v4/packages/npm/
//forgemia.inra.fr/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}
```

Install the npm module:

```bash
export CI_JOB_TOKEN=cBZySZds******
npm install --save @metexplore/metexplore3-api-types
```

Example of use in your code:

```typescript
import { DatabaseRef } from '@metexplore/metexplore3-api-types/dist';
...
const databaseRef: DatabaseRef = {
                                id: row['dbId'],
                                name: row['dbName'],
                                type: row['dbType'],
                                source: row['dbSource'],
                                url: row['dbUrl']
                            };
```

## How to contribute to the code ?

### Clone the repository

The git project contains one submodule corresponding to the openapi specifications. To get the git repo and the submodule, launch:

```console
git clone --recursive https://forgemia.inra.fr/metexplore/metexplore-api/metexplore3-api-types.git
```

### Install the main dependencies and init the submodule

```console
npm run init
```

### Build

Build the openapi specification bundle file and generate the types.
The generated types are not versioned since they are generated automatically. The following command will create a dist directory containing all the generated types:

```console
npm run build
```